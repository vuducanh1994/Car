#define ECO 1
#define NORMAL 2
#define SPORT 3

#include <iostream>
#include <string>

class DriverMode {
public:
    int mode;
    int previousMode;  // Thêm biến này để lưu giữ giá trị cũ

    DriverMode() : mode(0), previousMode(-1) {}  // Khởi tạo giá trị ban đầu

    // Hàm để nhận giá trị driver mode
    void setDriverMode(int newMode) {
        if (newMode != mode) {
            previousMode = mode;  // Lưu giữ giá trị cũ
            mode = newMode;
        }
    }

    // Hàm xử lý logic và in ra thông tin về driver mode
    void processDriverMode() {
        // Chỉ in ra khi có sự thay đổi
        if (mode != previousMode) {
            // Xử lý logic dựa trên giá trị driver mode
            std::string modeString;
            switch (mode) {
                case ECO:
                    modeString = "Mode Eco";
                    break;
                case NORMAL:
                    modeString = "Mode Normal";
                    break;
                case SPORT:
                    modeString = "Mode Sport";
                    break;
                default:
                    modeString = "Unknown Mode";
            }

            // In ra thông tin
            std::cout << "Current Driver Mode: " << modeString << std::endl;

            // Cập nhật giá trị cũ
            previousMode = mode;
        }
    }
};
