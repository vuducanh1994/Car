#define P (1)
#define D (2)


#include <iostream>
#include <string>

class Gear {
public:
    int gearPosition;
    int previousGearPosition;  // Thêm biến này để lưu giữ giá trị cũ

    Gear() : gearPosition(0), previousGearPosition(-1) {}  // Khởi tạo giá trị ban đầu

    // Hàm để nhận giá trị gearPosition
    void setGearPosition(int newPosition) {
        if (newPosition != gearPosition) {
            previousGearPosition = gearPosition;  // Lưu giữ giá trị cũ
            gearPosition = newPosition;
        }
    }

    // Hàm xử lý logic và in ra thông tin khi có sự thay đổi
    void processGear() {
        // Chỉ in ra khi có sự thay đổi
        if (gearPosition != previousGearPosition) {
            // Xử lý logic dựa trên giá trị gearPosition
            std::string gear;
            if (gearPosition == P) {
                gear = "Gear P";
            } else if (gearPosition == D) {
                gear = "Gear D";
            } else {
                gear = "Unknown Mode";
            }

            // In ra thông tin
            std::cout << "Current Gear Mode: " << gear << std::endl;

            // Cập nhật giá trị cũ
            previousGearPosition = gearPosition;
        }
    }
};
