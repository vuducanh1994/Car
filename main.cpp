#include "gear.cpp"
#include "configReader.cpp"
#include "speed.cpp"
#include "driverMode.cpp"

int main() {
    Gear gear;
    Speed speed;
    DriverMode driverMode;
    ConfigReader configReader("config.txt");



    std::cout << "----- Đang chạy giả lập xe -----" << std::endl;
    while (true) {
        // Đọc giá trị từ config.txt và thiết lập cho các chức năng
        gear.setGearPosition(configReader.readGearPosition());
        speed.setSpeed(configReader.readSpeed());
        driverMode.setDriverMode(configReader.readDriverMode());


        // Xử lý logic và in ra thông tin khi có sự thay đổi
        gear.processGear();
        speed.processSpeed();
        driverMode.processDriverMode();
        
    }

    return 0;
}
