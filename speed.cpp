#include <iostream>

class Speed {
public:
    int speed;
    int previousSpeed;  // Thêm biến này để lưu giữ giá trị cũ

    Speed() : speed(0), previousSpeed(-1) {}  // Khởi tạo giá trị ban đầu

    // Hàm để nhận giá trị speed
    void setSpeed(int newSpeed) {
        if (newSpeed != speed) {
            previousSpeed = speed;  // Lưu giữ giá trị cũ
            speed = newSpeed;
        }
    }

    // Hàm xử lý logic và in ra thông tin và cảnh báo khi có sự thay đổi
    void processSpeed() {
        // Chỉ in ra khi có sự thay đổi
        if (speed != previousSpeed) {
            // In ra thông tin
            std::cout << "Current Speed: " << speed << " km/h" << std::endl;

            // Cảnh báo nếu tốc độ vượt quá 120
            if (speed > 120) {
                std::cout << "Warning: Driving too fast!" << std::endl;
            }

            // Cập nhật giá trị cũ
            previousSpeed = speed;
        }
    }
};
