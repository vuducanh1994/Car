#include <fstream>
#include <sstream>
#include <unordered_map>

class ConfigReader {
public:
    ConfigReader(const std::string& filename) : filename(filename) {}

    // Đọc giá trị từ file config.txt
    int readGearPosition();
    int readSpeed();
    int readDriverMode();

private:
    std::string filename;
};

int ConfigReader::readGearPosition() {
    std::ifstream file(filename);
    std::string line;
    int gearPosition = 0;  // Giá trị mặc định hoặc giá trị không hợp lệ

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string variable;
        int value;

        // Đọc biến và giá trị từ mỗi dòng
        if (iss >> variable >> value && variable == "gear") {
            gearPosition = value;
            break;  // Chỉ cần đọc giá trị đầu tiên
        }
    }

    return gearPosition;
}

int ConfigReader::readSpeed() {
    std::ifstream file(filename);
    std::string line;
    int speed = 0;  // Giá trị mặc định hoặc giá trị không hợp lệ

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string variable;
        int value;

        // Đọc biến và giá trị từ mỗi dòng
        if (iss >> variable >> value && variable == "speed") {
            speed = value;
            break;  // Chỉ cần đọc giá trị đầu tiên
        }
    }

    return speed;
}

int ConfigReader::readDriverMode() {
    std::ifstream file(filename);
    std::string line;
    int driverMode = 0;  // Giá trị mặc định hoặc giá trị không hợp lệ

    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string variable;
        int value;

        // Đọc biến và giá trị từ mỗi dòng
        if (iss >> variable >> value && variable == "driverMode") {
            driverMode = value;
            break;  // Chỉ cần đọc giá trị đầu tiên
        }
    }

    return driverMode;
}
